/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView, 
  StatusBar,
} from 'react-native';
import CaptureImage from './src/CaptureImage'



const App=() => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView>
          <CaptureImage />
        </ScrollView>
      </SafeAreaView>
    </>
  );
};



export default App;
