/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useRef, useState } from 'react';
import { SafeAreaView, StatusBar, TouchableOpacity, Image, Text, ScrollView, View } from 'react-native';
import { SignatureView } from 'react-native-signature-capture-view';

const ViewImage = () => {
    const signatureRef = useRef(null);
    const [path, setPath] = useState('')
    return (
        <>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={{ flex: 1 }}>
                <SignatureView
                    style={{ height: 200, }}
                    ref={signatureRef}
                    onSave={(val) => {
                        console.log('saved signature')
                        console.log(val);
                        setPath(val)

                    }}
                    onClear={() => {
                        console.log('cleared signature')
                        setPath('')
                    }}

                />
                <View style={{ flexDirection: 'row', justifyContent: 'center', height: 50 }}>
                    <TouchableOpacity
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}
                        onPress={() => {
                            signatureRef.current.clearSignature();
                        }}>
                        <Text>Clear</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}
                        onPress={() => {
                            signatureRef.current.saveSignature();
                        }}>
                        <Text>Save</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView style={{ flex: 1, margin: 20 }}>
                    {/* <Text  ellipsizeMode='tail'> path :{path}</Text> */}

                    <Image
                        resizeMode={'contain'}
                        style={{ width: 300, height: 300 }}
                        source={{ uri: path }}
                    />




                </ScrollView>
            </SafeAreaView>
        </>
    );
};

export default ViewImage;