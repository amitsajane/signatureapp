/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
/* eslint-disable no-alert */
/* eslint-disable prettier/prettier */
// 


import React, { createRef, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  Alert,
  TouchableHighlight,
} from 'react-native';

import SignatureCapture from 'react-native-signature-capture';
import RNFetchBlob from "react-native-fetch-blob";


const CaptureImage = () => {
  const [path, setPath] = useState('')
  const sign = createRef();

  const saveSign = () => {
    sign.current.saveImage();
  };

  const resetSign = () => {
    sign.current.resetImage();
  };

  const _onSaveEvent = (result) => {
    // result.encoded 
    // result.pathName 
    // setPath(result.pathName)
    // alert('Signature Captured Successfully');
    // console.log(setPath);

    RNFetchBlob.fs
      .readFile(result.pathName, result.encoded, "encoding type")
      .then(success => {
        Alert.alert(
          "info",
          `It's been downloaded in ${result.pathName}.`
        );
      })
      .catch(err => {
        console.warn(err)
      });
    console.log(result.pathName);
    setPath(result.pathName)

  };

  const _onDragEvent = () => {
    console.log('dragged');
  };



  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <SignatureCapture
          style={styles.signature}
          ref={sign}
          onSaveEvent={_onSaveEvent}
          onDragEvent={_onDragEvent}
          showNativeButtons={false}
          showTitleLabel={false}
          viewMode={'portrait'}
          saveImageFileInExtStorage={true}
        />
        
        <View style={{ flexDirection: 'row' }}>
          <TouchableHighlight
            style={styles.buttonStyle}
            onPress={() => {
              saveSign();
            }}>
            <Text>Save</Text>
          </TouchableHighlight>
          <TouchableHighlight
            style={styles.buttonStyle}
            onPress={() => {
              resetSign();
            }}>
            <Text>Reset</Text>
          </TouchableHighlight>
        </View>


      </View>
    </SafeAreaView>
  );
};
export default CaptureImage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  titleStyle: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  signature: {
    flex: 1,
    height: 200,
    borderColor: '#000033',
    borderWidth: 1,
  },
  buttonStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: '#eeeeee',
    margin: 10,
  },
});